package mylearn.com.mvvmdemo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import mylearn.com.mvvmdemo.R;

public class AddStudentActivity extends AppCompatActivity {
    public static final String EXTRA_ID = "id";
    public static final String EXTRA_NAME = "name";
    public static final String EXTRA_CLASS = "class";

    EditText ed_name, ed_class;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_student);

        ed_name = (EditText) findViewById(R.id.ed_name);
        ed_class = (EditText) findViewById(R.id.ed_class);
        Button btnSubmit = (Button) findViewById(R.id.btn_submit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveStudent();
            }
        });

        Intent intent = getIntent();

        if (intent.hasExtra(EXTRA_ID)) {
            setTitle("Edit Student");
            ed_name.setText(intent.getStringExtra(EXTRA_NAME));
            ed_class.setText(intent.getStringExtra(EXTRA_CLASS));
        } else {
            setTitle("Add Student");
        }
    }

    private void saveStudent() {
        String student_name = ed_name.getText().toString();
        String student_class = ed_class.getText().toString();

        if (student_name.trim().isEmpty() || student_class.trim().isEmpty()) {
            Toast.makeText(this, "Please insert a name and class", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent data = new Intent();
        data.putExtra(EXTRA_NAME, student_name);
        data.putExtra(EXTRA_CLASS, student_class);

        int id = getIntent().getIntExtra(EXTRA_ID, -1);
        if (id != -1) {
            data.putExtra(EXTRA_ID, id);
        }

        setResult(RESULT_OK, data);
        finish();
    }
}
