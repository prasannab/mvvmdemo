package mylearn.com.mvvmdemo.activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.List;

import mylearn.com.mvvmdemo.R;
import mylearn.com.mvvmdemo.adapters.StudentAdapter;
import mylearn.com.mvvmdemo.db.StudentEntity;
import mylearn.com.mvvmdemo.viewmodels.StudentViewModel;

public class StudentActivity extends AppCompatActivity {
    private static final String TAG = "MVVM";
    public static final int ADD_STUDENT = 100;
    public static final int EDIT_STUDENT = 101;

    StudentViewModel viewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_std);

        FloatingActionButton buttonAddNote = findViewById(R.id.button_add_note);
        buttonAddNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StudentActivity.this, AddStudentActivity.class);
                startActivityForResult(intent, ADD_STUDENT);
            }
        });

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        final StudentAdapter studAdapter = new StudentAdapter();
        recyclerView.setAdapter(studAdapter);

        viewModel = ViewModelProviders.of(this).get(StudentViewModel.class);
        viewModel.listStudents().observe(this, new Observer<List<StudentEntity>>() {
            @Override
            public void onChanged(@Nullable List<StudentEntity> studentEntities) {
                // update view
                Log.d(TAG, "onChanged: list size" + studentEntities.size());
                studAdapter.setStudentList(studentEntities);
            }
        });

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                viewModel.deleteStudent(studAdapter.getNoteAt(viewHolder.getAdapterPosition()));
                Toast.makeText(StudentActivity.this, "Student deleted", Toast.LENGTH_SHORT).show();
            }
        }).attachToRecyclerView(recyclerView);


        studAdapter.setStudentItemClickListener(new StudentAdapter.OnStudentItemClickListener(){
            @Override
            public void onStudentItemClicked(StudentEntity student) {
                Intent intent = new Intent(StudentActivity.this, AddStudentActivity.class);
                intent.putExtra(AddStudentActivity.EXTRA_ID, student.getStudentId());
                intent.putExtra(AddStudentActivity.EXTRA_NAME, student.getName());
                intent.putExtra(AddStudentActivity.EXTRA_CLASS, student.getStudentClass());
                startActivityForResult(intent, EDIT_STUDENT);
            }
        });

        
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ADD_STUDENT && resultCode == RESULT_OK) {
            String sname = data.getStringExtra(AddStudentActivity.EXTRA_NAME);
            String sclass = data.getStringExtra(AddStudentActivity.EXTRA_CLASS);

            StudentEntity student = new StudentEntity(sname, sclass);
            viewModel.insertStudent(student);

            Toast.makeText(this, "Student saved", Toast.LENGTH_SHORT).show();
        } else if (requestCode == EDIT_STUDENT && resultCode == RESULT_OK) {
            int id = data.getIntExtra(AddStudentActivity.EXTRA_ID, -1);

            if (id == -1) {
                Toast.makeText(this, "Note can't be updated", Toast.LENGTH_SHORT).show();
                return;
            }

            String sname = data.getStringExtra(AddStudentActivity.EXTRA_NAME);
            String sclass = data.getStringExtra(AddStudentActivity.EXTRA_CLASS);

            StudentEntity note = new StudentEntity(sname, sclass);
            note.setStudentId(id);
            viewModel.updateStudent(note);

            Toast.makeText(this, "Student updated", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Student not saved", Toast.LENGTH_SHORT).show();
        }
    }
}
