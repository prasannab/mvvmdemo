package mylearn.com.mvvmdemo.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mylearn.com.mvvmdemo.R;
import mylearn.com.mvvmdemo.db.StudentEntity;

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.StudentHolder> {

    List<StudentEntity> studentList = new ArrayList<>();
    OnStudentItemClickListener listener;
    @NonNull
    @Override
    public StudentHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.student_item, viewGroup, false);
        return new StudentHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StudentHolder studentHolder, int pos) {
        StudentEntity student = studentList.get(pos);
        studentHolder.studentname.setText(student.getName());
        studentHolder.studentclass.setText(student.getStudentClass());
    }

    class StudentHolder extends RecyclerView.ViewHolder {
        private TextView studentname;
        private TextView studentclass;

        public StudentHolder(View view) {
            super(view);
            studentname = view.findViewById(R.id.text_name);
            studentclass = view.findViewById(R.id.text_class);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        listener.onStudentItemClicked(studentList.get(position));
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return studentList.size();
    }

    public void setStudentList(List<StudentEntity> list) {
        studentList = list;
        notifyDataSetChanged();
    }

    public StudentEntity getNoteAt(int position) {
        return studentList.get(position);
    }

    public interface OnStudentItemClickListener {
        void onStudentItemClicked(StudentEntity student);
    }

    public void setStudentItemClickListener(OnStudentItemClickListener listener) {
        this.listener = listener;
    }
}
