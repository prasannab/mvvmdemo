package mylearn.com.mvvmdemo.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

@Database(entities = {StudentEntity.class}, version = 1, exportSchema = false)
public abstract class StudentDB extends RoomDatabase {

    public static StudentDB studentDB;

    public abstract StudentDAO getStudentDAO();

    public static synchronized StudentDB getDBInstance(Context context) {
        if (studentDB == null) {
            studentDB = Room.databaseBuilder(context.getApplicationContext(),
                    StudentDB.class, "student_db").fallbackToDestructiveMigration().build();
        }
        return studentDB;
    }

    /*private static RoomDatabase.Callback callback = new RoomDatabase.Callback(){
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PreLoadAsyncTask(studentDB).execute();
        }
    };

    private static class PreLoadAsyncTask extends AsyncTask<StudentEntity, Void, Void> {
        StudentDAO dao;
        public PreLoadAsyncTask(StudentDB db) {
            this.dao = db.getStudentDAO();
        }

        @Override
        protected Void doInBackground(StudentEntity... studentEntities) {
            dao.insertStudent(new StudentEntity("prasanna", "1 std"));
            dao.insertStudent(new StudentEntity("subash", "2 std"));
            dao.insertStudent(new StudentEntity("raj", "3 std"));
            return null;
        }
    }*/
}
