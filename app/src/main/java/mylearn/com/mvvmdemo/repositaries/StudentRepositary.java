package mylearn.com.mvvmdemo.repositaries;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import mylearn.com.mvvmdemo.db.StudentDAO;
import mylearn.com.mvvmdemo.db.StudentDB;
import mylearn.com.mvvmdemo.db.StudentEntity;

public class StudentRepositary {
    private static final String TAG = "MVVM";
    StudentDAO studentDAO;
    LiveData<List<StudentEntity>> studentList;


    public StudentRepositary(Application application) {
        Log.d(TAG, "StudentRepositary: constructor()");
        StudentDB db = StudentDB.getDBInstance(application);
        studentDAO = db.getStudentDAO();
        studentList = studentDAO.getAllStudent();
        Log.d(TAG, "StudentRepositary: constructor(). studentList " + studentList);
    }

    public void insertStudent(StudentEntity student) {
        new InsertAsyncTask(studentDAO).execute(student);
    }

    public void deleteStudent(StudentEntity student) {
        new DeleteAsyncTask(studentDAO).execute(student);
    }

    public void updateStudent(StudentEntity student) {
        new UpdateAsyncTask(studentDAO).execute(student);
    }

    public LiveData<List<StudentEntity>> getAllStudent() {
        Log.i(TAG, "StudentRepositary() getAllStudent: " + studentList);
        return studentList;
    }

    private class InsertAsyncTask extends AsyncTask<StudentEntity, Void, Void> {
        StudentDAO dao;
        public InsertAsyncTask(StudentDAO dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(StudentEntity... studentEntities) {
            Log.i(TAG, "StudentRepositary() InsertAsyncTask");
            dao.insertStudent(studentEntities[0]);
            return null;
        }
    }

    private class DeleteAsyncTask extends AsyncTask<StudentEntity, Void, Void> {
        StudentDAO dao;
        public DeleteAsyncTask(StudentDAO dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(StudentEntity... studentEntities) {
            Log.i(TAG, "StudentRepositary() DeleteAsyncTask");
            dao.deleteStudent(studentEntities[0]);
            return null;
        }
    }

    private class UpdateAsyncTask extends AsyncTask<StudentEntity, Void, Void> {
        StudentDAO dao;
        public UpdateAsyncTask(StudentDAO dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(StudentEntity... studentEntities) {
            Log.i(TAG, "StudentRepositary() UpdateAsyncTask");
            dao.updateStudent(studentEntities[0]);
            return null;
        }
    }

}
