package mylearn.com.mvvmdemo.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import mylearn.com.mvvmdemo.db.StudentEntity;
import mylearn.com.mvvmdemo.repositaries.StudentRepositary;

public class StudentViewModel extends AndroidViewModel {
    private static final String TAG = "MVVM";
    StudentRepositary studentRepositary;
    LiveData<List<StudentEntity>> studentList;

    public StudentViewModel(Application application) {
        super(application);
        Log.i(TAG, "StudentViewModel: constructor()");
        studentRepositary = new StudentRepositary(application);
        studentList = studentRepositary.getAllStudent();
        Log.i(TAG, "StudentViewModel: constructor(). Studentlist size " + studentList);
    }

    public void insertStudent(StudentEntity student) {
        Log.i(TAG, "StudentViewModel: insertStudent()");
        studentRepositary.insertStudent(student);
    }

    public void deleteStudent(StudentEntity student) {
        Log.i(TAG, "StudentViewModel: delete student");
        studentRepositary.deleteStudent(student);
    }

    public void updateStudent(StudentEntity student) {
        Log.i(TAG, "StudentViewModel: update student");
        studentRepositary.updateStudent(student);
    }

    public LiveData<List<StudentEntity>> listStudents() {
        Log.i(TAG, "StudentViewModel: liststudent()");
        return studentList;
    }

    public void testInsert() {




        List<StudentEntity> list = new ArrayList<>();
        StudentEntity one = new StudentEntity("one", "1 std");
        one.setStudentId(1);
        list.add(one);

        StudentEntity two = new StudentEntity("two", "1 std");
        two.setStudentId(2);
        list.add(two);

        StudentEntity three = new StudentEntity("three", "1 std");
        three.setStudentId(3);
        list.add(three);


        //studentList.setData(list);


    }
}
